export function getIssuePossibleActions(oldStatus, newStatus) {
  return (
    oldStatus === newStatus ||
    oldStatus === "open" ||
    (oldStatus === "pending" && newStatus === "closed")
  );
}
