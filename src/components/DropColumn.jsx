import styled from "styled-components";
import Issue from "./Issue";
import {useDrop} from "react-dnd";
import PropTypes from "prop-types";
import {getIssuePossibleActions} from "../utils";

const DropSection = styled.div`
  position: relative;
  display: flex;
  height: 30em;
  width: 15em;
  padding: 1em;
  flex-direction: column;
  border-radius: 1em;
  border: ${(props) =>
    props.border && !props.canDrop ? "1px solid #E22B77CB" : "1px solid blueviolet"};
  box-shadow: ${(props) =>
    props.border ? (props.canDrop ? "0 0 1em blueviolet" : "0 0 1em #E22B77CB") : "none"};
`;

const Label = styled.h3`
  display: flex;
  font-weight: bold;
  align-items: center;
  justify-content: center;
  margin-top: 0;
`;

const Scrollable = styled.div`
  display: flex;
  flex-direction: column;
  gap: 1em;
  padding-bottom: 1.5em;
  overflow-y: scroll;
  scrollbar-width: none;
  -ms-overflow-style: none;
  ::-webkit-scrollbar {
    width: 0;
    height: 0;
  }
`;

const StyledEnd = styled.div`
  background-image: linear-gradient(transparent, white);
  height: 2em;
  width: 100%;
  position: absolute;
  bottom: 1em;
  left: 0;
  display: flex;
`;

const DropColumn = ({label, color, issues, setIssues}) => {
  const lowerCaseLabel = label.toLowerCase();

  const filterByStatus = (element) => {
    return element.status === lowerCaseLabel;
  };
  const replaceIssueHandler = (dragIndex, hoverIndex) => {
    const filteredIssues = issues.filter(filterByStatus);
    const dragItem = filteredIssues[dragIndex];
    const hoverItem = filteredIssues[hoverIndex];
    const newDragIndex = issues.indexOf(dragItem);
    const newHoverIndex = issues.indexOf(hoverItem);
    if (dragItem) {
      setIssues((prevState) => {
        const copiedIssues = [...prevState];
        const prevItem = copiedIssues.splice(newHoverIndex, 1, dragItem);
        copiedIssues.splice(newDragIndex, 1, prevItem[0]);
        return copiedIssues;
      });
    }
  };

  const [{isOver, canDrop}, drop] = useDrop({
    accept: "Issue",
    drop: () => ({status: lowerCaseLabel}),
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
    canDrop: (item) => {
      const {status} = item;
      return getIssuePossibleActions(status, lowerCaseLabel);
    },
  });

  return (
    <DropSection ref={drop} border={isOver} canDrop={canDrop}>
      <Label>{label}</Label>
      <Scrollable>
        {issues.filter(filterByStatus).map((issue, index) => (
          <Issue
            key={issue.id}
            index={index}
            issue={issue}
            setItems={setIssues}
            color={color}
            replaceIssueHandler={replaceIssueHandler}
          />
        ))}
      </Scrollable>
      <StyledEnd />
    </DropSection>
  );
};

DropColumn.propTypes = {
  label: PropTypes.string,
  color: PropTypes.string,
  issues: PropTypes.array,
  setIssues: PropTypes.func,
};

export default DropColumn;
