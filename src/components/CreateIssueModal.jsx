import styled, {css} from "styled-components";
import {useState} from "react";
import {STATUS_TYPES} from "../constants";

const ModalBackground = styled.div`
  visibility: ${(props) => (props.show ? "visible" : "hidden")};
  opacity: ${(props) => (props.show ? "1" : "0")};
  z-index: ${(props) => (props.show ? "10" : "-1")};
  position: absolute;
  top: 0;
  left: 0;
  bottom: 0;
  background: rgba(153, 153, 153, 0.7);
  width: 100%;
  height: 100%;
  transition: all 1s ease;
`;

const StyledModal = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 1em;
  background: #fff;
  visibility: ${(props) => (props.show ? "visible" : "hidden")};
  opacity: ${(props) => (props.show ? "1" : "0")};
  z-index: ${(props) => (props.show ? "100" : "-1")};
  top: ${(props) => (props.show ? "20%" : "-50%")};
  transition: ${(props) => (props.show ? "all 0.5s ease-in-out" : "none")};
  position: relative;
  margin: 0 auto;
  max-width: 30em;
  padding: 2em;
`;

const ModalTitle = styled.h3`
  font-size: 1.5em;
  font-weight: bold;
  color: #a554f3;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const StyledLabel = styled.label`
  display: flex;
  flex-direction: column;
  padding-bottom: 1em;
`;

const RadioLabel = styled(StyledLabel)`
  background: ${(props) => (props.checked ? props.color : "#fff")};
  padding: 1em;
  border-radius: 1em;
  line-height: 100%;
  box-sizing: border-box;
  :hover {
    background: ${(props) => props.color};
    opacity: 0.8;
  }
`;
const formStyle = css`
  display: flex;
  border: 1px solid #a554f3;
  border-radius: 5px;
  padding: 0.5em;
  :focus {
    outline: none;
    box-shadow: 0 0 0.2em rgba(163, 75, 234, 0.8);
  }
`;

const StyledInput = styled.input`
  ${formStyle};
  margin-top: 0.5em;
`;

const StyledTextarea = styled.textarea`
  ${formStyle};
  margin-top: 0.5em;
  resize: none;
  font-family: sans-serif;
`;

const StyledRadioWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  gap: 1em;
`;

const CustomRadioButton = styled.input.attrs({type: "radio"})`
  display: none;
`;

const CloseButton = styled.div`
  position: absolute;
  right: 1.5em;
  top: 1em;
  cursor: pointer;
`;

const ErrorMessage = styled.div`
  display: flex;
  color: #f66565;
  align-items: center;
  justify-content: center;
  padding-top: 1em;
`;

const CreateButton = styled.input.attrs({type: "submit"})`
  display: flex;
  justify-content: center;
  border-radius: 1em;
  color: rgba(144, 43, 226, 0.7);
  border: none;
  background-color: rgba(144, 43, 226, 0.26);
  padding: 0.5em;
  margin-top: 1em;
  width: 100%;
  font-size: 1em;
  text-transform: uppercase;
  :hover {
    background: #fff;
    box-shadow: 0 0 0.2em rgba(163, 75, 234, 0.8);
    transition: all 0.3s;
  }
`;

const CreateIssueModal = ({show, setShowModal, setIssues}) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [status, setStatus] = useState("open");
  const [validationError, setValidationError] = useState("");
  const addCard = (event) => {
    event.preventDefault();
    if (!title || !description) {
      const validationMessage =
        (!title ? (description ? "Title" : "Title & description") : "Description") +
        " cannot be empty!";
      setValidationError(validationMessage);
      return;
    }
    setIssues((prevState) => {
      const copiedIssues = [...prevState];
      const ids = copiedIssues.map((issue) => issue.id);
      const newId = Math.max(...ids) + 1;
      const newCard = {id: newId, title: title, description: description, status: status};
      return [...prevState, newCard];
    });
    setShowModal(false);
    setTitle("");
    setDescription("");
    setStatus("open");
    setValidationError("");
  };

  return (
    <ModalBackground show={show} onClick={() => setShowModal(false)}>
      <StyledModal show={show} onClick={(e) => e.stopPropagation()}>
        <ModalTitle>New issue</ModalTitle>
        <form onSubmit={addCard}>
          <StyledLabel>
            Title:
            <StyledInput
              type="text"
              value={title}
              placeholder={"Type something..."}
              onChange={(e) => setTitle(e.target.value)}
            />
          </StyledLabel>
          <StyledLabel>
            Description:
            <StyledTextarea
              value={description}
              placeholder={"Add something here..."}
              onChange={(e) => setDescription(e.target.value)}
              rows="4"
              cols="50"
            />
          </StyledLabel>
          Status:
          <StyledRadioWrapper>
            {STATUS_TYPES.map((statusItem) => {
              return (
                <RadioLabel
                  key={"radio_" + statusItem.type}
                  checked={status === statusItem.type.toLowerCase()}
                  color={statusItem.color}
                >
                  <CustomRadioButton
                    type="radio"
                    checked={status === statusItem.type.toLowerCase()}
                    onChange={(e) => setStatus(statusItem.type.toLowerCase())}
                  />
                  {statusItem.type}
                </RadioLabel>
              );
            })}
          </StyledRadioWrapper>
          {validationError && <ErrorMessage>{validationError}</ErrorMessage>}
          <CreateButton value="Create" />
        </form>
        <CloseButton onClick={() => setShowModal(false)}>x</CloseButton>
      </StyledModal>
    </ModalBackground>
  );
};

export default CreateIssueModal;
