import styled from "styled-components";
import {useDrag, useDrop} from "react-dnd";
import {useRef} from "react";
import Bin from "../assets/bin.svg";
import PropTypes from "prop-types";
import {getIssuePossibleActions} from "../utils";

const StyledIssue = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 1em;
  padding: 1em;
  cursor: pointer;
  background-color: ${(props) => props.backgroundColor || "#c898eea3"};
`;
const IssueHeader = styled.div`
  display: flex;
`;
const IssueTitle = styled.div`
  display: flex;
  font-weight: bold;
  font-size: 0.9em;
  width: calc(100% - 1em);
`;
const Description = styled.div`
  padding-top: 1em;
  font-size: 0.8em;
`;

const DeleteImage = styled.img`
  :hover {
    transform: scale(1.1, 1.1);
  }
`;

const Issue = ({
  index,
  issue: {id, title, description, status},
  color,
  setItems,
  replaceIssueHandler,
}) => {
  const deleteIssue = () => {
    setItems((prevState) => {
      return prevState.filter((item) => {
        return item.id !== id;
      });
    });
  };

  const changeItemColumn = (currentItem, status) => {
    setItems((prevState) => {
      return prevState.map((e) => {
        return {
          ...e,
          status: e.id === currentItem.id ? status : e.status,
        };
      });
    });
  };

  const ref = useRef(null);

  const [, drop] = useDrop({
    accept: "Issue",
    canDrop: (item) => {
      const newStatus = item.status;
      return getIssuePossibleActions(status, newStatus);
    },
    drop(item, monitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;
      if (dragIndex === hoverIndex) {
        return;
      }
      const hoverBoundingRect = ref.current?.getBoundingClientRect();
      const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
      const clientOffset = monitor.getClientOffset();
      const hoverClientY = clientOffset.y - hoverBoundingRect.top;
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }
      replaceIssueHandler(dragIndex, hoverIndex);
      item.index = hoverIndex;
    },
  });

  const [, drag] = useDrag({
    type: "Issue",
    item: {index, id, title, description, status},
    end: (item, monitor) => {
      const dropResult = monitor.getDropResult();
      if (dropResult) {
        const {status} = dropResult;
        changeItemColumn(item, status);
      }
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  drag(drop(ref));

  return (
    <StyledIssue ref={ref} backgroundColor={color}>
      <IssueHeader>
        <IssueTitle>{title}</IssueTitle>
        <div onClick={deleteIssue}>
          <DeleteImage src={Bin} alt="Delete issue icon" />
        </div>
      </IssueHeader>
      <Description>{description}</Description>
    </StyledIssue>
  );
};

Issue.propTypes = {
  index: PropTypes.number,
  issue: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    description: PropTypes.string,
    status: PropTypes.string,
  }),
  color: PropTypes.string,
  setItems: PropTypes.func,
  replaceIssueHandler: PropTypes.func,
};

export default Issue;
