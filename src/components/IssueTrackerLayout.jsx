import React, {useEffect, useState} from "react";
import {DndProvider} from "react-dnd";
import {HTML5Backend} from "react-dnd-html5-backend";
import axios from "axios";
import styled from "styled-components";
import DropColumn from "./DropColumn";
import AddButton from "./AddButton";
import CreateIssueModal from "./CreateIssueModal";
import {STATUS_TYPES} from "../constants";

const PageLayout = styled.div`
  display: flex;
  padding: 1em;
  flex-direction: column;
`;
const ArticlePage = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 2em;
  padding: 1em;
  @media (max-width: 480px) {
    flex-direction: column;
  }
`;

const IssueTrackerLayout = () => {
  const [issues, setIssues] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [error, setError] = useState("");
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    axios
      .get(`http://localhost:8080/api/issues`)
      .then((response) => {
        setIssues(response.data.issues);
        setError("");
        setIsLoading(false);
      })
      .catch((error) => {
        setError("Something went wrong... Try again later");
        setIsLoading(false);
      });
  }, []);

  return (
    <PageLayout>
      <AddButton setShowModal={setShowModal} />
      <ArticlePage>
        {!error && !isLoading ? (
          <DndProvider backend={HTML5Backend}>
            {STATUS_TYPES.map((status) => {
              return (
                <DropColumn
                  key={status.type}
                  label={status.type}
                  color={status.color}
                  issues={issues}
                  setIssues={setIssues}
                />
              );
            })}
          </DndProvider>
        ) : isLoading ? (
          ""
        ) : (
          error
        )}
      </ArticlePage>
      <CreateIssueModal
        show={showModal}
        setShowModal={setShowModal}
        setIssues={setIssues}
      />
    </PageLayout>
  );
};

export default IssueTrackerLayout;
