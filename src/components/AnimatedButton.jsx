import styled from "styled-components";

export const AnimatedButton = styled.div`
  border-radius: 50%;
  line-height: 100%;
  text-align: center;
  width: 1em;
  height: 1em;
  font-size: 2em;
  cursor: pointer;
  color: rgba(144, 43, 226, 0.7);
  transition: all 0.5s;
  position: relative;
  margin-right: 0.5em;
  ::before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 1em;
    height: 1em;
    z-index: 1;
    border-radius: 50%;
    background-color: rgba(144, 43, 226, 0.26);
    transition: all 0.3s;
  }
  :hover::before {
    opacity: 0;
    transform: scale(0.5, 0.5);
  }
  ::after {
    content: "";
    position: absolute;
    top: -1px;
    left: -1px;
    width: 1em;
    height: 1em;
    z-index: 1;
    opacity: 0;
    transition: all 0.3s;
    border-radius: 50%;
    border: 1px solid rgba(163, 75, 234, 0.62);
    box-shadow: 0 0 0.2em rgba(163, 75, 234, 0.8);
    transform: scale(1.3, 1.3);
  }
  :hover::after {
    opacity: 1;
    transform: scale(1, 1);
  }
`;
