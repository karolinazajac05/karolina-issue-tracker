import styled from "styled-components";
import {AnimatedButton} from "./AnimatedButton";
import PropTypes from "prop-types";

const StyledButton = styled.button`
  border: none;
  background: none;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 1em;
  padding-bottom: 1em;
`;
const AddButton = ({setShowModal}) => {
  return (
    <StyledButton onClick={() => setShowModal(true)}>
      <AnimatedButton>+</AnimatedButton>
      Create new issue
    </StyledButton>
  );
};

AddButton.propTypes = {
  setShowModal: PropTypes.func,
};

export default AddButton;
