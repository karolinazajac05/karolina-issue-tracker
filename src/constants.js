export const STATUS_TYPES = [
  {type: "Open", color: "#c7f69da3"},
  {type: "Pending", color: "#ffa50061"},
  {type: "Closed", color: "#c898eea3"},
];
