import IssueTrackerLayout from "./components/IssueTrackerLayout";
import styled from "styled-components";

const AppContainer = styled.div`
  font-family: "Open Sans", sans-serif;
`;

const Header = styled.h2`
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
  font-family: "Staatliches", cursive;
  font-size: 2em;
`;

function App() {
  return (
    <AppContainer>
      <Header>Karolina Issue Tracker</Header>
      <IssueTrackerLayout />
    </AppContainer>
  );
}

export default App;
