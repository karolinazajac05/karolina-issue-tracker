## Karolina Issue Tracker App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
I created simple endpoint which only serves mock data, it could be done only on frontend but I added this because I want
to practise backend development and I plan to finish api later, so you can dismiss api folder.
I created kanban board with DnD react package and added functionality for adding new issues and deleting existing 
ones. 
To improve this app I could add functionality of editing issues, work more on design part, rewrite it in 
typescript, add tests.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

## Server

To run server with mock data run this in the project directory:

### `node server.js`
