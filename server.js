const http = require("http");

const fakeIssues = [
  {id: 1, title: "Go for a bike", description: "Today or tomorrow", status: "open"},
  {id: 2, title: "Eat a burger", description: "Nothing more here", status: "open"},
  {
    id: 3,
    title: "Drink tea",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    status: "pending",
  },
  {id: 4, title: "Drink water", description: "What what what", status: "closed"},
  {
    id: 5,
    title: "Go to sleep",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    status: "closed",
  },
];
const PORT = 8080;

function requestHandler(req, res) {
  let content;
  let status;

  if (req.url === "/api/issues") {
    content = {issues: fakeIssues};
    status = 200;
  } else {
    content = {message: "Not found"};
    status = 404;
  }

  res.setHeader("Access-Control-Allow-Origin", "*"); // CORS * for simplicity
  res.setHeader("Content-Type", "application/json"); // always json
  res.writeHead(status);
  res.end(JSON.stringify(content), "utf-8");
}

http.createServer(requestHandler).listen(PORT, (err) => {
  if (err) {
    console.log("something bad happened", err);
    return;
  }
  console.log(`server is listening on port: ${PORT}`);
});
