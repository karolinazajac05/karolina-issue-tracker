module.exports = (app) => {
  const issues = require("../controllers/issue.controller.js");
  const router = require("express").Router();
  // Create a new Issue
  router.post("/", issues.create);
  // Retrieve all Issues
  router.get("/", issues.findAll);
  // Retrieve a single Issue with id
  router.get("/:id", issues.findOne);
  // Update an Issue with id
  router.put("/:id", issues.update);
  // Delete an Issue with id
  router.delete("/:id", issues.delete);
  app.use("/api/issues", router);
};
