const db = require("../models");
const Issue = db.issues;

// Create and Save a new Issue
exports.create = (req, res) => {
  // Validate request
  if (!req.body.title) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
    return;
  }
  // Create an Issue
  const issue = new Issue({
    title: req.body.title,
    description: req.body.description,
    status: req.body.status,
  });
  // Save Issue in the database
  Issue.save(issue)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the issue.",
      });
    });
};
// Retrieve all Issues from the database.
exports.findAll = (req, res) => {
  Issue.find()
    .then((data) => {
      const sampleIssues = [
        {id: 1, title: "Blop", description: "What what whattt", status: "open"},
        {id: 2, title: "Blop2", description: "What what whattt3", status: "open"},
        {id: 3, title: "Blop3", description: "What what whattt4", status: "pending"},
        {id: 4, title: "Blop4", description: "What what whattt5", status: "pending"},
        {id: 5, title: "Blop5", description: "What what whattt6", status: "closed"},
        {id: 6, title: "Blop6", description: "What what whattt7", status: "closed"},
      ];
      res.send(sampleIssues);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving issues.",
      });
    });
};
// Find a single Issue with an id
exports.findOne = (req, res) => {
  const id = req.params.id;
  Issue.findById(id)
    .then((data) => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({
          message: `Cannot find Issue with id=${id}.`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error retrieving Issue with id=" + id,
      });
    });
};
// Update an Issue by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }
  const id = req.params.id;
  Issue.findByIdAndUpdate(id, req.body, {useFindAndModify: false})
    .then((data) => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update Issue with id=${id}. Maybe Issue was not found!`,
        });
      } else res.send({message: "Issue was updated successfully."});
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating Issue with id=" + id,
      });
    });
};
// Delete an Issue with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;
  Issue.findByIdAndRemove(id)
    .then((data) => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete Issue with id=${id}. Maybe Issue was not found!`,
        });
      } else {
        res.send({
          message: "Issue was deleted successfully!",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete Issue with id=" + id,
      });
    });
};
